﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NewsDao;
using NewsDao.Context;
using NewsWeb.Data;
using Quartz;
using System.Collections.Specialized;
using Quartz.Impl;
using NewsWeb.JobScheduler;
using System.Threading;


//using NewsDao.Context;

namespace NewsWeb
{
    public class Program
    {
        private static IScheduler _scheduler; // add this field
        public static void Main(string[] args)
        {
            // BuildWebHost(args).Run();
            var host = BuildWebHost(args);

            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                try
                {
                    var context = services.GetRequiredService<ApplicationDbContext>();
                    RssBackgroundProcess.RssProcess();
                   
                }
                catch (Exception ex)
                {
                    var logger = services.GetRequiredService<ILogger<Program>>();
                    logger.LogError(ex, "An error occurred while seeding the database.");
                }
            }

            host.Run();
        }
       
        

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
            .UseSetting("detailedErrors", "true")
                .UseStartup<Startup>()
          
            .CaptureStartupErrors(true)
                .Build();
    }
}
