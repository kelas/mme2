﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using NewsDao.Account;
using NewsDao.entities;
using NewsWeb.Models;

namespace NewsWeb.Data
{
    public class ApplicationDbContext : IdentityDbContext<NewsDao.Account.ApplicationUser>
    {
        



        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Category> Category { get; set; }
        public DbSet<Country> Country { get; set; }
        public DbSet<Metadata> Metadata { get; set; }
        public DbSet<Company> Company { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            //  base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
            // modelBuilder.Entity<Category>().ToTable("Category");
            // modelBuilder.Entity<Country>().ToTable("Country");
            //modelBuilder.Entity<Metadata>().ToTable("Metadata");

            //modelBuilder.Entity<Category>().HasOne(a => a.Metadata)
            //    .WithOne(b => b.Category)
            //    .HasForeignKey<Metadata>(b => b.CategoryId);
        }
    }
}
