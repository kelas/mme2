﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System;
using System.IO;
using System.Web;
using Microsoft.AspNetCore.Http;
using NewsDao.dto;

namespace NewsWeb.Models
{
    public class CompanyModel
    {
        public int Id { get; set; }
        public string LogoPath { get; set; }
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }
        [Required(ErrorMessage = "UserName is required")]
        [DisplayName("User Name")]
        public string UserName { get; set; }
        public int CountryId { get; set; }

        public IEnumerable<SelectListItem> Countries { get; set; }
        public List<IFormFile> files { get; set; }
    }
    public class CompanyViewModel
    {
        public List<CompanyDto> Companies { get; set; }
    }
}
