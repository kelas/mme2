﻿using NewsDao.dto;
using NewsDao.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewsWeb.Models
{
    public class RssModel
    {
       public List<Feed> Feeds { get; set; }
       public string rssUrl { get; set; }
    }
}
