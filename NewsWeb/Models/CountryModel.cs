﻿using NewsDao.dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NewsWeb.Models
{
    public class CountryModel
    {
        
        public int Id { get; set; }
        public string Title { get; set; }
        public bool IsActive { get; set; }
    }
    public class CountryListModel
    {
        public List<CountryDto> CountryDto { get; set; }
    }
}
