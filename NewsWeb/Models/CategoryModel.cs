﻿using NewsDao.dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewsWeb.Models
{
    public class CategoryModel
    {
        public int Id { get; set; }
        public string Title { get; set; }        
        public bool IsActive { get; set; }
    }
    public class CategoryListModel
    {
        public List<CategoryDto> CategoryDto { get; set; }
    }
}
