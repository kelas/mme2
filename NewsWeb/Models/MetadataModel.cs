﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using NewsDao.dto;

namespace NewsWeb.Models
{
    public class MetadataModel
    {
        public int Id { get; set; }
        public string RssPath { get; set; }
        public int CompanyId { get; set; }
        public int CategoryId { get; set; }
        public int CountryId { get; set; }
        public IEnumerable<SelectListItem> Categories { get; set; }
        public IEnumerable<SelectListItem> Companies { get; set; }
        public IEnumerable<SelectListItem> Countries { get; set; }
    }
    public class MetadataListModel
    {
        public List<MetadataDto> MetadataDto { get; set; }
    }
}
