﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NewsDao.services;
using NewsDao.entities;

namespace NewsWeb.Controllers
{
    public class JobSchedulerController : Controller
    {
        private readonly IMetadataService _metadataService;
        private readonly IRssDataService _rssDataService;
        private readonly IRssDataBackupService _rssDataBackupService;
        private readonly IExchangeRateService _exchangeRateService;

        public JobSchedulerController(IMetadataService metadataService, IRssDataService rssDataService, IRssDataBackupService rssDataBackupService, IExchangeRateService exchangeRateService)
        {
            _metadataService = metadataService;
            _rssDataService = rssDataService;
            _rssDataBackupService = rssDataBackupService;
            _exchangeRateService = exchangeRateService;
        }
        public IActionResult Index()
        {
            return View();
        }
        public async Task<ActionResult> StartJob()
        {
            try
            {
                var metadata = _metadataService.SelectAll();
                // await System.Threading.Tasks.Task.WhenAll(metadata.Select(t=> _rssDataService.InsertDataFromRssAsync(t)));
                foreach (var data in metadata)
                {
                    var result = await  _rssDataService.InsertDataFromRssAsync(data);
                }
            }
            catch (Exception ex)
            {
                // todo logs
            }
           

            return new EmptyResult();
        }

        public ActionResult CleanOldRss()
        {
            try
            {
                var data = _rssDataService.SelectSimpleQuery(new CriteriaRssData {

                }).Where(x=>x.PublishDate < DateTime.Now.AddDays(-2)).ToList();
                if(data.Count() > 0)
                {
                    foreach(var d in data)
                    {
                        var backup = new RssDataBackup();
                        backup.RssDataId = d.Id;
                        backup.Link = d.Link;
                        backup.Title = d.Title;
                        backup.Content = d.Content;
                        backup.PublishDate = d.PublishDate;
                        backup.CompanyId = d.CompanyId;
                        backup.CategoryId = d.CategoryId;
                        backup.CountryId = d.CountryId;
                        backup.BackUpDate = DateTime.Now;

                        _rssDataBackupService.SaveOrUpdate(backup);
                        _rssDataService.RemoveEntity(d);

                    }
                }
            }
            catch (Exception ex)
            {

            }
            return new EmptyResult();
        }

        public async Task<ActionResult> ExchangeRate()
        {
            try
            {
                //euro to dollar
                var EUR_USD =await _exchangeRateService.PopulateDataAsync("http://free.currencyconverterapi.com/api/v5/convert?q=USD_EUR&compact=y", "USD_EUR");
                var eD = _exchangeRateService.SelectSimpleQuery(new CriteriaExchangeRate
                {
                    Rate = "USD_EUR"
                }).FirstOrDefault();
                eD.Value = System.Convert.ToDecimal(EUR_USD);
                _exchangeRateService.SaveOrUpdate(eD);
                //euro to pound
                var EUR_GBP = await _exchangeRateService.PopulateDataAsync("http://free.currencyconverterapi.com/api/v5/convert?q=GBP_EUR&compact=y", "GBP_EUR");
                var eG = _exchangeRateService.SelectSimpleQuery(new CriteriaExchangeRate
                {
                    Rate = "GBP_EUR"
                }).FirstOrDefault();
                eG.Value = System.Convert.ToDecimal(EUR_GBP);
                _exchangeRateService.SaveOrUpdate(eG);
                //bitcoin to euro
                var BTC_EUR = await _exchangeRateService.PopulateDataAsync("http://free.currencyconverterapi.com/api/v5/convert?q=BTC_EUR&compact=y", "BTC_EUR");
                var bE = _exchangeRateService.SelectSimpleQuery(new CriteriaExchangeRate
                {
                    Rate = "BTC_EUR"
                }).FirstOrDefault();
                bE.Value = System.Convert.ToDecimal(BTC_EUR);
                _exchangeRateService.SaveOrUpdate(bE);

                //oil
                var oil = await _exchangeRateService.GetOilData("https://mediametrics.ru/quotes/top/currency_out.js");
                var oB = _exchangeRateService.SelectSimpleQuery(new CriteriaExchangeRate
                {
                    Rate = "OIL_USD"
                }).FirstOrDefault();
                oB.Value = System.Convert.ToDecimal(oil);
                _exchangeRateService.SaveOrUpdate(oB);
            }
            catch (Exception ex)
            {

            }
            
            return new EmptyResult();
        }
    }
}