﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NewsWeb.Models;
using NewsDao.services;
using System.IO;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.AspNetCore.Http.Features;

namespace NewsWeb.Controllers
{
    public class HomeController : Controller
    {
        private readonly IRssDataService _rssDataService;
        private readonly IExchangeRateService _exchangeRateService;

        public HomeController(IRssDataService rssDataService, IExchangeRateService exchangeRateService)
        {
            _rssDataService = rssDataService;
            _exchangeRateService = exchangeRateService;
        }
        public IActionResult Index()
        {
          //  var remote = this.HttpContext.Features.Get<IHttpConnectionFeature>()?.RemoteIpAddress;
            return View();
        }

        public JsonResult GetNews(int category = 0,int time = 0, int page = 0)
        {
            try
            {
                var data = _rssDataService.SelectSimleDto(new CriteriaRssData
                {
                    TimeInterval = time,
                    CategoryId = category,
                    Page = page
                });
                return Json(data);
            }
            catch (Exception ex)
            {
                //using (StreamWriter writetext = new StreamWriter(@"h:\root\home\nkelesidis-001\www\site1\log.txt"))
                //{
                //    writetext.WriteLine(ex.ToString());
                //}
            }

            return null;
        }

        public JsonResult GetCurrencyRate()
        {
            var data = _exchangeRateService.SelectSimpleQuery(new CriteriaExchangeRate
            {

            }).ToList();
            return Json(data);
        }
        public JsonResult SetHr(int id)
        {
           // Startup.TimeIntervalHrs = id;
            return Json("{'SetHr':'true'}"); 
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
