﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using NewsDao.Account;
using NewsDao.Context;
using NewsDao.entities;
using NewsDao.services;
using NewsWeb.Models;
using NewsWeb.Models.AdminViewModel;
using NewsDao.services.RSS;
using System.Net.Http;
using System.Xml.Linq;
using NewsDao.dto;

namespace NewsWeb.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private readonly UserManager<NewsDao.Account.ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ICountryService _countryService;
        private readonly ICompanyService _companyService;
        private readonly ICategoryService _categoryService;
        private readonly IMetadataService _metadataService;
        private readonly IRssReedFeed _rssReedFeed;
        private IHostingEnvironment _env;


        public AdminController(UserManager<NewsDao.Account.ApplicationUser> userManager, RoleManager<IdentityRole> roleManager, ICountryService countryService,
            IHostingEnvironment env, ICompanyService companyService, ICategoryService categoryService, IMetadataService metadataService, IRssReedFeed rssReedFeed)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _countryService = countryService;
            _env = env;
            _companyService = companyService;
            _categoryService = categoryService;
            _metadataService = metadataService;
            _rssReedFeed = rssReedFeed;
        }
        public async Task<IActionResult> Index()
        {


                return View();
        }

        public ActionResult AddRole()
        {

            return View();
        }
        [HttpPost]
        public async Task<ActionResult> AddRole(RoleModel model)
        {
            if (ModelState.IsValid)
            {
                bool x = await _roleManager.RoleExistsAsync(model.Title);
                if (!x)
                {
                    var role = new IdentityRole();
                    role.Name = model.Title;
                    await _roleManager.CreateAsync(role);
                    //var result1 = await _userManager.AddToRoleAsync(user, "Admin");
                }
            }
            return RedirectToAction("AddRole");
        }

        public ActionResult AddCompany()
        {
            var contries = _countryService.SelectAll();
            var countryList = contries.Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.Title
            });
            var model = new CompanyModel
            {
                Countries = countryList
            };
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> AddCompany(CompanyModel model)
        {
            if (ModelState.IsValid)
            {
                string contentRootPath = _env.ContentRootPath;
              //  var parentName = Directory.GetParent(contentRootPath).FullName;
                var logoPath = contentRootPath + "/wwwroot/Logo/"+model.CountryId+"/" + model.UserName;
                System.IO.Directory.CreateDirectory(logoPath);

                using (var fileStream = new FileStream(Path.Combine(logoPath, "logo.png"), FileMode.Create))
                {
                    await model.files[0].CopyToAsync(fileStream);
                }

                var company = new Company
                {
                    UserName = model.UserName,
                    CountryId = model.CountryId,
                    Name = model.Name,
                    LogoPath = "Logo/" + model.CountryId + "/" + model.UserName+"/"
            };

                _companyService.SaveOrUpdate(company);
                return RedirectToAction("AddCompany");

            }
           
                var contries = _countryService.SelectAll();
                var countryList = contries.Select(x => new SelectListItem
                {
                    Value = x.Id.ToString(),
                    Text = x.Title
                });
                model.Countries = countryList;
                          
            return View(model);
        }

        public ActionResult Metadata()
        {
            var model = new MetadataModel();
            //categories
            var categories = _categoryService.SelectAll();
            var categoryList = categories.Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.Title
            });
            model.Categories = categoryList;
            //countries
            var contries = _countryService.SelectAll();
            var countryList = contries.Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.Title
            });
            model.Countries = countryList;
            //companies
            var companies = _companyService.SelectAll();
            var companyList = companies.Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.Name
            });
            model.Companies = companyList;

            return View(model);
        }
        [HttpPost]
        public ActionResult Metadata(MetadataModel form)
        {
            if (ModelState.IsValid)
            {
                var metadata = new Metadata();
                metadata.CategoryId = form.CategoryId;
              //  metadata.Category = _categoryService.GetById(form.CategoryId);

                metadata.CompanyId = form.CompanyId;
               // metadata.Company = _companyService.GetById(form.CompanyId);

                metadata.CountryId = form.CountryId;
               // metadata.
                metadata.RssPath = form.RssPath;
                _metadataService.SaveOrUpdate(metadata);
            }
            return RedirectToAction("Metadata");
        }

        public ActionResult CheckRss()
        {

            return View();
        }
        [HttpPost]
        public async Task<ActionResult> CheckRss(RssModel form)
        {
            if (ModelState.IsValid)
            {
                var rssItem = await _rssReedFeed.ReedRssAsync(form.rssUrl);
                var model = new RssModel
                {
                    Feeds = rssItem.ToList(),
                };
                return View(model);
            }
                return View();
        }


    }
}