﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NewsDao.services;
using NewsWeb.Models;
using NewsDao.entities;

namespace NewsWeb.Controllers.Administrator
{
    public class CountryController : Controller
    {
        public readonly ICountryService _countryService;
        public CountryController(ICountryService countryService)
        {
            _countryService = countryService;
        }
        public IActionResult Index()
        {
            return RedirectToAction("Country");
        }
        public ActionResult Country()
        {
            var dto = _countryService.SelectSimpleDto(new CriteriaCountry {
            });
            var model = new CountryListModel()
            {
                CountryDto = dto,
            };
            return View(model);
        }
        public ActionResult AddCountry(int id = 0)
        {
            var model = new CountryModel();
            if(id > 0)
            {
                var entity = _countryService.GetById(id);
                model.Id = id;
                model.Title = entity.Title;
                model.IsActive = entity.IsActive;
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult AddCountry(CountryModel model)
        {
           
                var entity = new Country()
                {
                    Id = model.Id,
                    Title = model.Title,
                    IsActive = model.IsActive
                };
                _countryService.SaveOrUpdate(entity);
            

            return RedirectToAction("Country");
        }
        public ActionResult Delete(int id)
        {
            var item = _countryService.GetById(id);
            _countryService.RemoveEntity(item);

            return RedirectToAction("Country");
        }
    }
}