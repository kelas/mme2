﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using NewsWeb.Models;
using NewsDao.services;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using Microsoft.AspNetCore.Mvc.Rendering;
using NewsDao.entities;

namespace NewsWeb.Controllers.Administrator
{
    [Authorize(Roles = "Admin")]
    public class CompanyController : Controller
    {
        private readonly ICompanyService _companyService;
        private IHostingEnvironment _env;
        private readonly ICountryService _countryService;
        public CompanyController(ICompanyService companyService, IHostingEnvironment env, ICountryService countryService)
        {
            _companyService = companyService;
            _env = env;
            _countryService = countryService;
        }
        public IActionResult Index()
        {
            return  RedirectToAction("Company");
        }

        public IActionResult Company()
        {
            var dto = _companyService.SelectSimpleDto(new CrteriaCompany {

            });
            var model = new CompanyViewModel
            {
                Companies = dto
            };

          return  View(model);
        }

        public ActionResult Delete(int id)
        {
            var item = _companyService.GetById(id);
            try
            {
                //delete folder
                string contentRootPath = _env.ContentRootPath;
                var logoPath = contentRootPath + "/wwwroot/Logo/" + item.CountryId + "/" + item.UserName;
                Directory.Delete(logoPath,true);
                //delete from db
                _companyService.RemoveEntity(item);

            }
            catch (Exception ex)
            {

            }
            

            return  RedirectToAction("Company");
        }
        public ActionResult AddCompany()
        {
            var contries = _countryService.SelectAll();
            var countryList = contries.Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.Title
            });
            var model = new CompanyModel
            {
                Countries = countryList
            };
            return View(model);
        }
        [HttpPost]
        public async Task<ActionResult> AddCompany(CompanyModel model)
        {
            if (ModelState.IsValid)
            {
                string contentRootPath = _env.ContentRootPath;
                //  var parentName = Directory.GetParent(contentRootPath).FullName;
                var logoPath = contentRootPath + "/wwwroot/Logo/" + model.CountryId + "/" + model.UserName;
                System.IO.Directory.CreateDirectory(logoPath);

                using (var fileStream = new FileStream(Path.Combine(logoPath, "logo.png"), FileMode.Create))
                {
                    await model.files[0].CopyToAsync(fileStream);
                }

                var company = new Company
                {
                    UserName = model.UserName,
                    CountryId = model.CountryId,
                    Name = model.Name,
                    LogoPath = "Logo/" + model.CountryId + "/" + model.UserName + "/"
                };

                _companyService.SaveOrUpdate(company);
                return RedirectToAction("Company");

            }

            var contries = _countryService.SelectAll();
            var countryList = contries.Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.Title
            });
            model.Countries = countryList;

            return View(model);
        }
    }
}