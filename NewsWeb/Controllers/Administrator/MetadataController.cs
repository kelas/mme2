﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NewsWeb.Models;
using NewsDao.entities;
using NewsDao.services;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace NewsWeb.Controllers.Administrator
{
    public class MetadataController : Controller
    {
        private readonly ICountryService _countryService;
        private readonly ICompanyService _companyService;
        private readonly ICategoryService _categoryService;
        private readonly IMetadataService _metadataService;

        public MetadataController(ICountryService countryService, ICompanyService companyService,
            ICategoryService categoryService, IMetadataService metadataService)
        {
            _countryService = countryService;
            _companyService = companyService;
            _categoryService = categoryService;
            _metadataService = metadataService;
        }
        public IActionResult Index()
        {
            return RedirectToAction("Metadata");
        }
        public ActionResult AddMetadata()
        {
            var model = new MetadataModel();
            //categories
            var categories = _categoryService.SelectAll();
            var categoryList = categories.Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.Title
            });
            model.Categories = categoryList;
            //countries
            var contries = _countryService.SelectAll();
            var countryList = contries.Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.Title
            });
            model.Countries = countryList;
            //companies
            var companies = _companyService.SelectAll();
            var companyList = companies.Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.Name
            });
            model.Companies = companyList;

            return View(model);
        }
        [HttpPost]
        public ActionResult AddMetadata(MetadataModel form)
        {
            if (ModelState.IsValid)
            {
                var metadata = new Metadata();
                metadata.CategoryId = form.CategoryId;
                //  metadata.Category = _categoryService.GetById(form.CategoryId);

                metadata.CompanyId = form.CompanyId;
                // metadata.Company = _companyService.GetById(form.CompanyId);

                metadata.CountryId = form.CountryId;
                // metadata.
                metadata.RssPath = form.RssPath;
                _metadataService.SaveOrUpdate(metadata);
            }
            return RedirectToAction("Metadata");
        }

        public ActionResult Metadata()
        {
            var dto = _metadataService.SelectSimpleDto(new CriteriaMetadata
            {

            }).ToList();

            var model = new MetadataListModel
            {
                MetadataDto = dto
            };
            return View(model);

        }
        public ActionResult Delete(int id)
        {
            var item = _metadataService.GetById(id);
            _metadataService.RemoveEntity(item);

            return RedirectToAction("Metadata");
        }

    }
}