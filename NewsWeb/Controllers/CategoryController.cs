﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NewsDao.services;
using NewsWeb.Models;
using NewsDao.entities;

namespace NewsWeb.Controllers
{
    public class CategoryController : Controller
    {
        public readonly ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }
        public IActionResult Index()
        {
            return RedirectToAction("Category");
        }
        public ActionResult Category()
        {
            var dto = _categoryService.SelectSimpleDto(new CriteriaCategory
            {
            });
            var model = new CategoryListModel()
            {
                CategoryDto = dto,
            };
            return View(model);
        }
        public ActionResult AddCategory(int id = 0)
        {
            var model = new CategoryModel();
            if(id > 0)
            {
                var entity = _categoryService.GetById(id);
                model.Id = id;
                model.Title = entity.Title;
                model.IsActive = entity.IsActive;
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult AddCategory(CategoryModel model)
        {

            var entity = new Category()
            {
                Id = model.Id,
                Title = model.Title,
                IsActive = model.IsActive
            };

            _categoryService.SaveOrUpdate(entity);

            return RedirectToAction("Category");
        }

        public ActionResult Delete(int id)
        {
            var entity = _categoryService.GetById(id);
            _categoryService.RemoveEntity(entity);

            return RedirectToAction("Category");
        }
    }
}