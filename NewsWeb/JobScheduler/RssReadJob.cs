﻿using NewsDao.services;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using System.Net.Http;
using Microsoft.AspNetCore.Http;

namespace NewsWeb.JobScheduler
{
    public class RssReadJob 
    {

         //string url = "http://nkelesidis-001-site1.itempurl.com/";
         string url = "http://localhost:62441/";
        public async Task ExecuteAsync()
        {
            using (var client = new HttpClient())
            {
                var dest = url + "JobScheduler/startjob";
                client.BaseAddress = new Uri(dest);
                var responseMessage = await client.GetAsync(dest);

            }


        }

        public async Task CleanOldRss()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var dest = url + "JobScheduler/CleanOldRss";
                    client.BaseAddress = new Uri(dest);
                    var responseMessage = await client.GetAsync(dest);
                }
            }
            catch(Exception ex)
            {

            }
        }

        public async Task CurrencyRateJob()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var dest = url + "JobScheduler/ExchangeRate";
                    client.BaseAddress = new Uri(dest);
                    var responseMessage = await client.GetAsync(dest);
                }
            }
            catch (Exception ex)
            {

            }
        }



    }
}
