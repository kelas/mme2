﻿using NewsDao.services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NewsWeb.JobScheduler
{
    public static class RssBackgroundProcess
    {
        private static bool run = true;
        static void mythread1Async()
        {
            var rssJob = new RssReadJob();
            while (run)
            {
                 rssJob.ExecuteAsync();
                Thread.Sleep(300000); // 5 min
            }
            
        }
        static void cleanRssThread()
        {
            var rssJob = new RssReadJob();
            while (run)
            {
                rssJob.CleanOldRss();
                Thread.Sleep(43200000); // 12 hours
            }
        }

        static void currencyRateJob()
        {
            var rssJob = new RssReadJob();
            while (run)
            {//CurrencyRateJob
                rssJob.CurrencyRateJob();
                Thread.Sleep(300000);
            }
        }
        public static void RssProcess()
        {
            Thread thread1 = new Thread(mythread1Async);
            thread1.Start();

            Thread thread2 = new Thread(cleanRssThread);
            thread2.Start();

            Thread thread3 = new Thread(currencyRateJob);
            thread3.Start();
        }

    }
}
