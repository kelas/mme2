"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var HttpService_1 = require("../../services/HttpService/HttpService");
var NewsService_1 = require("../../services/NewsService");
var LeftSideComponent = (function () {
    function LeftSideComponent(newsService) {
        this.newsService = newsService;
        //const screenHeight = window.screen.width;
    }
    LeftSideComponent.prototype.setCurrencyRate = function (data) {
        var _this = this;
        data.forEach(function (e) {
            if (e.rate && e.rate === "USD_EUR" && e.value > 0) {
                _this.USD_EUR = e.value;
            }
            if (e.rate && e.rate === "GBP_EUR" && e.value > 0) {
                _this.GBP_EUR = e.value;
            }
            if (e.rate && e.rate === "BTC_EUR" && e.value > 0) {
                _this.BTC_EUR = e.value;
            }
            if (e.rate && e.rate === "OIL_USD" && e.value > 0) {
                _this.OIL_USD = e.value;
            }
        });
    };
    LeftSideComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.screenWidth = window.screen.width;
        this.mySidenavWidth = true;
        if (this.screenWidth < 993) {
            this.mySidenavWidth = false;
            this.leftColumn = true;
            this.leftContentBtn = false;
        }
        else {
            this.mySidenavWidth = true;
            this.leftColumn = false;
            this.leftContentBtn = true;
        }
        this.newsService.getData().subscribe(function (data) {
            _this.data = data;
            _this.setCurrencyRate(_this.data);
        });
    };
    LeftSideComponent.prototype.onResize = function (event) {
        if (event.target.innerWidth < 993) {
            this.mySidenavWidth = false;
            this.leftColumn = true;
            this.leftContentBtn = false;
        }
        else {
            this.mySidenavWidth = true;
            this.leftColumn = false;
            this.leftContentBtn = true;
        }
        // console.log("width: "+event.target.innerWidth);
    };
    //navigation
    LeftSideComponent.prototype.openNav = function () {
        this.mySidenavWidth = true;
    };
    LeftSideComponent.prototype.closeNav = function () {
        this.mySidenavWidth = false;
    };
    return LeftSideComponent;
}());
LeftSideComponent = __decorate([
    core_1.Component({
        selector: 'left-side',
        providers: [NewsService_1.NewsService, HttpService_1.HttpService],
        templateUrl: './left-side.component.html',
        styleUrls: ['./left-side.component.css']
    }),
    __metadata("design:paramtypes", [NewsService_1.NewsService])
], LeftSideComponent);
exports.LeftSideComponent = LeftSideComponent;
//# sourceMappingURL=left-side.component.js.map