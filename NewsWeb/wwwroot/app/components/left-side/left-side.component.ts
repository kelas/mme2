import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpService } from "../../services/HttpService/HttpService";
import { NewsService } from "../../services/NewsService";


@Component({
    selector: 'left-side',
    providers: [NewsService, HttpService],
  templateUrl: './left-side.component.html',
  styleUrls: ['./left-side.component.css']
})
export class LeftSideComponent implements OnInit {

    constructor(private newsService: NewsService) {
        //const screenHeight = window.screen.width;
    }

    cureencyList: any;
    data: any;
    event: any;
    leftColumn: boolean;
    screenWidth: number;
    leftContentBtn: boolean;
    mySidenavWidth: boolean;

    //currency rate
    USD_EUR: number;
    GBP_EUR: number;
    BTC_EUR: number;
    OIL_USD: number;


    setCurrencyRate(data: any[]) {
        data.forEach(e => {
            if (e.rate && e.rate === "USD_EUR"  && e.value > 0) {
                this.USD_EUR = e.value;
            }
            if (e.rate && e.rate === "GBP_EUR"  && e.value > 0) {
                this.GBP_EUR = e.value;
            }
            if (e.rate && e.rate === "BTC_EUR"  && e.value > 0) {
                this.BTC_EUR = e.value;
            }
            if (e.rate && e.rate === "OIL_USD"  && e.value > 0) {
                this.OIL_USD = e.value;
            }

        });

    }
    ngOnInit() {
        this.screenWidth = window.screen.width;
        this.mySidenavWidth = true;

        if (this.screenWidth < 993) {
            this.mySidenavWidth = false;

            this.leftColumn = true;
            this.leftContentBtn = false;
        } else {
            this.mySidenavWidth = true;

            this.leftColumn = false;
            this.leftContentBtn = true;
        }

        this.newsService.getData().subscribe(data => {
            this.data = data;
            this.setCurrencyRate(this.data);
        });
        
        
    }
   
    onResize(event: any) {
        if (event.target.innerWidth < 993) {
            this.mySidenavWidth = false;
            this.leftColumn = true;
            this.leftContentBtn = false;
        } else {
            this.mySidenavWidth = true;
            this.leftColumn = false;
            this.leftContentBtn = true;
        }
       // console.log("width: "+event.target.innerWidth);
    }
    //navigation
    openNav() {
        this.mySidenavWidth = true;
    }
    closeNav() {
        this.mySidenavWidth = false;
    }
}
