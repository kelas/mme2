"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var NewsService_1 = require("../../services/NewsService");
var HttpService_1 = require("../../services/HttpService/HttpService");
var platform_browser_1 = require("@angular/platform-browser");
//import { DomSanitizer } from "@angular/platform-browser/bundles/platform-browser";
var NewsListComponent = (function () {
    //clicked: boolean;
    function NewsListComponent(newsService, sanitizer) {
        this.newsService = newsService;
        this.sanitizer = sanitizer;
        this.hr1clicked = true;
        this.hr2clicked = false;
        this.hr3clicked = false;
        this.cat1clicked = true;
        this.cat2clicked = false;
        this.cat3clicked = false;
        this.cat4clicked = false;
        this.cat5clicked = false;
        this.timeInterval = 1;
        this.category = 1;
        this.page = 1;
        this.clickBtn = function (link, id) {
            window.open(link, '_blank');
        };
    }
    NewsListComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (!this.totalNews) {
            this.totalNews = [];
        }
        if (!this.news) {
            this.news = [];
        }
        this.newsList = this.newsService.getNews(this.timeInterval, this.category, this.page);
        this.newsList.subscribe(function (data) {
            _this.news = data;
            if (_this.news) {
                _this.news.forEach(function (e) {
                    _this.totalNews.push(e);
                });
                // console.log("total: " + this.totalNews.length);
            }
        });
    };
    NewsListComponent.prototype.newPage = function () {
        this.page = this.page + 1;
        this.ngOnInit();
    };
    NewsListComponent.prototype.sanitize = function (url) {
        return this.sanitizer.bypassSecurityTrustUrl(url);
    };
    NewsListComponent.prototype.setBtnHrStyle = function (hr) {
        switch (hr) {
            case 1:
                this.hr1clicked = true;
                this.hr2clicked = false;
                this.hr3clicked = false;
                this.timeInterval = 1;
                break;
            case 12:
                this.hr1clicked = false;
                this.hr2clicked = true;
                this.hr3clicked = false;
                this.timeInterval = 12;
                break;
            case 24:
                this.hr1clicked = false;
                this.hr2clicked = false;
                this.hr3clicked = true;
                this.timeInterval = 24;
                break;
        }
    };
    NewsListComponent.prototype.setBtnCatStyle = function (ct) {
        switch (ct) {
            case 1:
                this.cat1clicked = true;
                this.cat2clicked = false;
                this.cat3clicked = false;
                this.cat4clicked = false;
                this.cat5clicked = false;
                this.category = 1;
                break;
            case 2:
                this.cat1clicked = false;
                this.cat2clicked = true;
                this.cat3clicked = false;
                this.cat4clicked = false;
                this.cat5clicked = false;
                this.category = 2;
                break;
            case 3:
                this.cat1clicked = false;
                this.cat2clicked = false;
                this.cat3clicked = true;
                this.cat4clicked = false;
                this.cat5clicked = false;
                this.category = 3;
                break;
            case 4:
                this.cat1clicked = false;
                this.cat2clicked = false;
                this.cat3clicked = false;
                this.cat4clicked = true;
                this.cat5clicked = false;
                this.category = 4;
                break;
            case 5:
                this.cat1clicked = false;
                this.cat2clicked = false;
                this.cat3clicked = false;
                this.cat4clicked = false;
                this.cat5clicked = true;
                this.category = 5;
                break;
            default:
                this.cat1clicked = true;
                this.cat4clicked = false;
                this.cat5clicked = false;
                this.category = 1;
                break;
        }
    };
    NewsListComponent.prototype.hrClick = function (hr) {
        this.page = 1;
        //this.totalNews == [];
        this.totalNews.length = 0;
        this.setBtnHrStyle(hr);
        this.ngOnInit();
    };
    NewsListComponent.prototype.ctrClick = function (ct) {
        this.totalNews.length = 0;
        this.setBtnCatStyle(ct);
        this.ngOnInit();
        // this.$('#title').click();
    };
    return NewsListComponent;
}());
NewsListComponent = __decorate([
    core_1.Component({
        selector: 'news-list',
        providers: [NewsService_1.NewsService, HttpService_1.HttpService],
        templateUrl: './news-list.component.html',
        styleUrls: ['./news-list.component.css']
    }),
    __metadata("design:paramtypes", [NewsService_1.NewsService, platform_browser_1.DomSanitizer])
], NewsListComponent);
exports.NewsListComponent = NewsListComponent;
//# sourceMappingURL=news-list.component.js.map