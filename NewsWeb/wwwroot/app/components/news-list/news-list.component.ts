import { Component, OnInit } from '@angular/core';
import { NewsService } from "../../services/NewsService";
import { HttpService } from "../../services/HttpService/HttpService";
import { Observable } from 'rxjs/Observable';
import { News } from "../../models/News";
import { DomSanitizer } from '@angular/platform-browser';
//import { DomSanitizer } from "@angular/platform-browser/bundles/platform-browser";

@Component({
    selector: 'news-list',
    providers: [NewsService,HttpService], 
    templateUrl: './news-list.component.html',
    styleUrls: ['./news-list.component.css']
})
export class NewsListComponent implements OnInit {

    newsList: Observable<News[]>
    news: News[];
    totalNews: News[];
    errorMessage: String;
    //clicked: boolean;
    constructor(private newsService: NewsService, public sanitizer: DomSanitizer) { }

    hr1clicked = true;
    hr2clicked = false;
    hr3clicked = false;

    cat1clicked = true;
    cat2clicked = false;
    cat3clicked = false;
    cat4clicked = false;
    cat5clicked = false;



    timeInterval = 1;
    category = 1;
    page = 1;
    $: any;

    ngOnInit() {

        if (!this.totalNews) {
            this.totalNews = [];
        }
        if (!this.news) {
            this.news = [];
        }

        this.newsList = this.newsService.getNews(this.timeInterval, this.category, this.page);       

        
        this.newsList.subscribe(data => {

            
            this.news = data;
            if (this.news) {
                this.news.forEach(e => {
                    this.totalNews.push(e);
                })

               // console.log("total: " + this.totalNews.length);
            }
                
            
        });
        
    }
    
    newPage() {
        this.page = this.page + 1;
        this.ngOnInit();
    }

    sanitize(url: string) {

        return this.sanitizer.bypassSecurityTrustUrl(url);
    }
    clickBtn =(link: string, id: any) =>{
        window.open(link, '_blank');
    }
    setBtnHrStyle(hr:any) {
       
        switch (hr) {
            case 1:
                this.hr1clicked = true;
                this.hr2clicked = false;
                this.hr3clicked = false;
                this.timeInterval = 1;
                break;
            case 12:
                this.hr1clicked = false;
                this.hr2clicked = true;
                this.hr3clicked = false;
                this.timeInterval = 12;
                break;
            case 24:
                this.hr1clicked = false;
                this.hr2clicked = false;
                this.hr3clicked = true;
                this.timeInterval = 24;
                break;

        }
        

    }
    setBtnCatStyle(ct: any) {

        switch (ct) {
            case 1:
                this.cat1clicked = true;
                this.cat2clicked = false;
                this.cat3clicked = false;
                this.cat4clicked = false;
                this.cat5clicked = false;
                this.category = 1;
                break;
            case 2:
                this.cat1clicked = false;
                this.cat2clicked = true;
                this.cat3clicked = false;
                this.cat4clicked = false;
                this.cat5clicked = false;
                this.category = 2;
                break;
            case 3:
                this.cat1clicked = false;
                this.cat2clicked = false;
                this.cat3clicked = true;
                this.cat4clicked = false;
                this.cat5clicked = false;
                this.category = 3;
                break;
            case 4:
                this.cat1clicked = false;
                this.cat2clicked = false;
                this.cat3clicked = false;
                this.cat4clicked = true;
                this.cat5clicked = false;
                this.category = 4;
                break;
            case 5:
                this.cat1clicked = false;
                this.cat2clicked = false;
                this.cat3clicked = false;
                this.cat4clicked = false;
                this.cat5clicked = true;
                this.category = 5;
                break;
            default:
                this.cat1clicked = true;
                this.cat4clicked = false;
                this.cat5clicked = false;
                this.category = 1;
                break;
        }

    }
    hrClick(hr: any) {
        this.page = 1;
        //this.totalNews == [];
        this.totalNews.length = 0;
        this.setBtnHrStyle(hr);
        this.ngOnInit();
        
    }
    ctrClick(ct: any) {
        this.totalNews.length = 0;

        this.setBtnCatStyle(ct);
        this.ngOnInit();
       // this.$('#title').click();
    }



}
