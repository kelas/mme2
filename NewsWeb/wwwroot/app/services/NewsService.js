"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var HttpService_1 = require("./HttpService/HttpService");
var NewsService = (function () {
    function NewsService($http) {
        this.$http = $http;
    }
    NewsService.prototype.getNews = function (time, category, page) {
        return this.$http.get("Home/GetNews/time?=" + time + "&category=" + category + "&page=" + page);
    };
    NewsService.prototype.getData = function () {
        return this.$http.get("Home/GetCurrencyRate/");
    };
    NewsService.prototype.setHrs = function (hr) {
        return this.$http.httpGet("Home/SetHr/?id=" + hr);
    };
    NewsService.prototype.dataFromExternalApi = function (url) {
        return this.$http.httpApi(url);
    };
    return NewsService;
}());
NewsService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [HttpService_1.HttpService])
], NewsService);
exports.NewsService = NewsService;
//# sourceMappingURL=NewsService.js.map