﻿

import { Injectable } from '@angular/core';
import { News } from '../models/News'
import { HttpService } from "./HttpService/HttpService";


@Injectable()
export class NewsService {

    
    constructor( private $http: HttpService) { }

    getNews(time: any, category: any, page: any) {
        return this.$http.get("Home/GetNews/time?=" + time + "&category=" + category + "&page="+page);
    }

    getData() {
        return this.$http.get("Home/GetCurrencyRate/");
    }

    setHrs(hr:any) {
        return this.$http.httpGet("Home/SetHr/?id="+hr);
    }

    dataFromExternalApi(url: any) {
        return this.$http.httpApi(url);
    }
   


}