﻿import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Injectable()
export class HttpService {
    //baseUrl = "http://nkelesidis-001-site1.itempurl.com/";
    baseUrl = "http://localhost:62441/";
    //baseUrl = "";
    data: any;
    constructor(private http: Http) { }

    getBaseUrl() {
        if (location.hostname === "localhost" || location.hostname === "127.0.0.1") {
            return "http://localhost:62441/";
        } else {
            return "http://nkelesidis-001-site1.itempurl.com/";
        }
    }

    get(url:string): Observable<any[]> {
        return this.http.get(this.getBaseUrl()+url)
            .map(this.extractData)
            .catch(this.handleErrorObservable);
    }
    httpGet(url: string) {
        return this.http.get(this.getBaseUrl() + url);
    }
    httpApi(url: string) {
        return this.http.get(url).map((res: Response) => res.json());
    }
    private extractData(res: Response) {
        let body = res.json();
        return body;
    }
    private handleErrorObservable(error: Response | any) {
        console.error(error.message || error);
        return Observable.throw(error.message || error);
    }
}