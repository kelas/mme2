﻿export class News {
    id: number;
    Link: string;
    Title: string;
    Content: string;
    PublishDate: Date;
    CompanyId: number;
    CategoryId: number;
    CountryId: number;

    constructor() {
    }
}