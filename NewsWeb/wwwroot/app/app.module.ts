import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { NewsListComponent } from './components/news-list/news-list.component';
import { LeftSideComponent } from './components/left-side/left-side.component';


@NgModule({
    imports: [BrowserModule, HttpModule ],
    declarations: [AppComponent, NewsListComponent, LeftSideComponent ],
  bootstrap: [AppComponent, NewsListComponent, LeftSideComponent ]
})
export class AppModule { }
