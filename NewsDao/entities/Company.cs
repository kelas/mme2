﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewsDao.entities
{
    public class Company
    {
        public int Id { get; set; }
        public string LogoPath { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public int CountryId { get; set; }
        public Country Country { get; set; }
        
    }
}
