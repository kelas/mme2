﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewsDao.entities
{
    public class ExchangeRate
    {
        public int Id { get; set; }
        public string Rate { get; set; }
        public decimal Value { get; set; }
        
    }
}
