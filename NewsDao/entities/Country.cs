﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewsDao.entities
{
    public class Country
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public bool IsActive { get; set; }
    }
}
