﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace NewsDao.entities
{
    public class Category
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public Metadata Metadata { get; set; }
        public bool IsActive { get; set; }
    }
}
