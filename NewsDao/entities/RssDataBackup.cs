﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewsDao.entities
{
    public class RssDataBackup
    {
        public int Id { get; set; }
        public string Link { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime PublishDate { get; set; }

        public int CompanyId { get; set; }
        public Company Company { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public int CountryId { get; set; }
        public Country Country { get; set; }
        public int RssDataId { get; set; }
        public DateTime BackUpDate { get; set; }
    }
}
