﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewsDao.entities
{
    public class Metadata
    {
        public int Id { get; set; }
        public string RssPath { get; set; }
        public int CompanyId { get; set; }
        public Company Company { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public int CountryId { get; set; }
        public Country Country { get; set; }

        public DateTime? LastUpdate { get; set; }

    }
}
