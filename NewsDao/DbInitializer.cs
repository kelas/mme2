﻿using NewsDao.entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
//using NewsDao.Context;

namespace NewsDao
{
    public static class DbInitializer
    {

        public static void Initialize(ApplicationDbContext context)
        {
            context.Database.EnsureCreated();

            //Category
            if (context.Category.Any())
            {
                return;
            }
            var category = new List<Category>();
            category.Add(new Category{Title = "General"});
            category.Add(new Category { Title = "Politics" });
            category.Add(new Category { Title = "Cars" });
            category.Add(new Category { Title = "Sport" });
            category.Add(new Category { Title = "The science" });
            foreach (var c in category)
            {
                context.Add(c);
            }
            context.SaveChanges();
            //Country
            if (context.Country.Any())
            {
                return;
            }

            var country = new List<Country>();
            country.Add(new Country { Title = "Greece" });
            country.Add(new Country { Title = "Russia" });
            country.Add(new Country { Title = "Usa" });
            foreach (var c in country)
            {
                context.Add(c);
            }
            context.SaveChanges();
        }

    }
}
