﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace NewsDao.Migrations
{
    public partial class Metadata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CountryId",
                table: "Metadata",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Metadata_CountryId",
                table: "Metadata",
                column: "CountryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Metadata_Country_CountryId",
                table: "Metadata",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Metadata_Country_CountryId",
                table: "Metadata");

            migrationBuilder.DropIndex(
                name: "IX_Metadata_CountryId",
                table: "Metadata");

            migrationBuilder.DropColumn(
                name: "CountryId",
                table: "Metadata");
        }
    }
}
