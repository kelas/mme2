﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace NewsDao.Migrations
{
    public partial class Role : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("INSERT INTO aspnetroles (Id,ConcurrencyStamp,Name,NormalizedName) VALUES ('6345ae47-6ae1-41ad-8603-eb6a5d9d2068','','Admin','ADMIN')");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("REMOVE FROM dbo.aspnetroles");
        }
    }
}
