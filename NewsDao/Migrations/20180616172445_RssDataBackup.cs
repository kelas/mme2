﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace NewsDao.Migrations
{
    public partial class RssDataBackup : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "RssDataBackup",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    BackUpDate = table.Column<DateTime>(nullable: false),
                    CategoryId = table.Column<int>(nullable: false),
                    CompanyId = table.Column<int>(nullable: false),
                    Content = table.Column<string>(nullable: true),
                    CountryId = table.Column<int>(nullable: false),
                    Link = table.Column<string>(nullable: true),
                    PublishDate = table.Column<DateTime>(nullable: false),
                    RssDataId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RssDataBackup", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RssDataBackup_Category_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Category",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RssDataBackup_Company_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RssDataBackup_Country_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Country",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_RssDataBackup_CategoryId",
                table: "RssDataBackup",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_RssDataBackup_CompanyId",
                table: "RssDataBackup",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_RssDataBackup_CountryId",
                table: "RssDataBackup",
                column: "CountryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RssDataBackup");
        }
    }
}
