﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace NewsDao.Migrations
{
    public partial class DropIndexMetadata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Metadata_Category_CategoryId",
                table: "Metadata");
            migrationBuilder.DropIndex(
                name: "IX_Metadata_CategoryId",
                table: "Metadata");
            migrationBuilder.AddForeignKey(
                name: "FK_Metadata_Category_CategoryId",
                table: "Metadata",
                column: "CategoryId",
                principalTable: "Category",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CategoryId",
                table: "Metadata");
        }
    }
}
