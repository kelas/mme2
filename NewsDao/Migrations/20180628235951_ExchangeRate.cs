﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace NewsDao.Migrations
{
    public partial class ExchangeRate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ExchangeRate",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Rate = table.Column<string>(nullable: true),
                    Value = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExchangeRate", x => x.Id);
                });
            migrationBuilder.Sql("INSERT INTO ExchangeRate (Id,Rate,Value) VALUES (1,'USD_EUR','0.0')");
            migrationBuilder.Sql("INSERT INTO ExchangeRate (Id,Rate,Value) VALUES (2,'GBP_EUR','0.0')");
            migrationBuilder.Sql("INSERT INTO ExchangeRate (Id,Rate,Value) VALUES (3,'BTC_EUR','0.0')");
            migrationBuilder.Sql("INSERT INTO ExchangeRate (Id,Rate,Value) VALUES (4,'OIL_USD','0.0')");
            migrationBuilder.Sql("INSERT INTO ExchangeRate (Id,Rate,Value) VALUES (5,'GOLD_USD','0.0')");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ExchangeRate");
        }
    }
}
