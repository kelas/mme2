﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace NewsDao.Migrations
{
    public partial class Update1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("INSERT INTO country (Id,Title) VALUES (1,'Greece')");
            migrationBuilder.Sql("INSERT INTO country (Id,Title) VALUES (2,'Russia')");
            migrationBuilder.Sql("INSERT INTO country (Id,Title) VALUES (3,'Usa')");
            migrationBuilder.Sql("INSERT INTO category (Id,Title) VALUES (1,'General')");
            migrationBuilder.Sql("INSERT INTO category (Id,Title) VALUES (2,'Politics')");
            migrationBuilder.Sql("INSERT INTO category (Id,Title) VALUES (3,'Cars')");
            migrationBuilder.Sql("INSERT INTO category (Id,Title) VALUES (4,'Sport')");
            migrationBuilder.Sql("INSERT INTO category (Id,Title) VALUES (5,'TheScience')");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("REMOVE FROM dbo.country");
            migrationBuilder.Sql("REMOVE FROM dbo.category");
        }
    }
}
