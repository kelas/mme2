﻿using NewsDao.Context;
using NewsDao.entities;
using NewsDao.services.RSS;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using NewsDao.dto;
using Microsoft.EntityFrameworkCore;

namespace NewsDao.services
{
    public interface IRssDataService:IBaseService<RssData>
    {
        System.Threading.Tasks.Task<bool> InsertDataFromRssAsync(Metadata metadata);
        IQueryable<RssData> SelectSimpleQuery(CriteriaRssData criteria);
        List<RssDataDto> SelectSimleDto(CriteriaRssData criteria);
    }
    public class RssDataService: BaseService<RssData>,IRssDataService
    {
        private readonly IRssReedFeed _rssReedFeed;
        private readonly IMetadataService _metadataService;

        private ApplicationDbContext cnx;

        public RssDataService(ApplicationDbContext context, IRssReedFeed rssReedFeed, IMetadataService metadataService) :base(context)
        {
            _rssReedFeed = rssReedFeed;
            _metadataService = metadataService;
            cnx = context;
        }
        
        public IQueryable<RssData> SelectSimpleQuery(CriteriaRssData criteria)
        {
            var query = Query();
            query = query.Where(x => x.Country.IsActive && x.Category.IsActive);//select only from active always

            if(criteria.Id > 0)
            {
                query = query.Where(x => x.Id == criteria.Id);
            }
            if(criteria.CompanyId > 0)
            {
                query = query.Where(x => x.CompanyId == criteria.CompanyId);
            }
            if(criteria.CountryId > 0)
            {
                query = query.Where(x => x.CountryId == criteria.CountryId);
            }
            if(criteria.CategoryId > 0)
            {
                query = query.Where(x => x.CategoryId == criteria.CategoryId);
            }

            

            if(criteria.TimeInterval > 0)
            {
                DateTime now = DateTime.Now;
                query = query.Where(x => x.PublishDate >= now.AddHours(-criteria.TimeInterval));
               
            }
            
            if (criteria.TimeInterval == 1)
            {
                query = query.OrderByDescending(x => x.PublishDate);
            }
            else
            {
                query = query.OrderBy(x => x.PublishDate);
            }
            if (criteria.Page > 0)
            {

                query = query.Take(criteria.Page * 100);
                if (criteria.Page > 1)
                {
                    query = query.Skip((criteria.Page - 1) * 100);
                }
            }
            return query;
        }
        public List<RssDataDto> SelectSimleDto(CriteriaRssData criteria)
        {

            var dto = SelectSimpleQuery(criteria).Select(x => new RssDataDto
            {
                Id = x.Id,
                Link = x.Link,
                Title = !string.IsNullOrEmpty(x.Title) && x.Title.Length > 100 ? x.Title.Substring(0, 100) + "..." : x.Title,
                Content = x.Content,
                PublishDate = x.PublishDate,
                CompanyId = x.CompanyId,
                CategoryId = x.CategoryId,
                CountryId = x.CountryId,
                Logo = x.Company.LogoPath
            }).ToList();
            return dto;
        }
        public async Task<bool> Save(Metadata metadata, IEnumerable<Feed> dataFromRss)
        {
            if (dataFromRss.Count() > 0)
            {
                if (metadata.LastUpdate.HasValue)
                {
                    dataFromRss = dataFromRss.Where(x => x.PublishDate > metadata.LastUpdate);
                }

                foreach (var rss in dataFromRss)
                {
                    try
                    {
                        var rssData = new RssData
                        {
                            Link = rss.Link,
                            Title = rss.Title,
                            PublishDate = rss.PublishDate,
                            Content = rss.Content,
                            CategoryId = metadata.CategoryId,
                            // Category = metadata.Category,
                            CompanyId = metadata.CompanyId,
                            //  Company = metadata.Company,
                            CountryId = metadata.CountryId,
                            // Country = metadata.Country

                        };


                 //       SaveOrUpdate(rssData);
                    }
                    catch (Exception ex)
                    {
                        // return false;
                    }

                }

                //save rss to stream
                if (dataFromRss != null && dataFromRss.Count() > 0)
                {
                    var lastUpdatedDate = dataFromRss.OrderByDescending(x => x.PublishDate).Select(x => x.PublishDate).FirstOrDefault();
                    if (lastUpdatedDate.Date.Year != DateTime.Now.Year)
                    {
                        lastUpdatedDate = DateTime.Now.AddHours(1);
                    }
                    // var lastUpdatedDate = DateTime.Now;
                    metadata.LastUpdate = lastUpdatedDate;
                //    _metadataService.SaveOrUpdate(metadata);
                }
            }
           
            return true;
        }
        public async System.Threading.Tasks.Task<bool> InsertDataFromRssAsync(Metadata metadata)
        {
            var dataFromRss =await _rssReedFeed.ReedRssAsync(metadata.RssPath);
            
             var result =  await Save(metadata,dataFromRss);

             return true;
            
        }

    }

    public class CriteriaRssData
    {
        public int Id { get; set; }
       
        public DateTime PublishDate { get; set; }

        public int CompanyId { get; set; }
        public Company Company { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public int CountryId { get; set; }
        public Country Country { get; set; }
        public int TimeInterval { get; set; }
        public int Page { get; set; }
    }
}
