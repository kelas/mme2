﻿using System;
using System.Collections.Generic;
using System.Text;
using NewsDao.Context;
using NewsDao.entities;
using System.Linq;
using NewsDao.dto;

namespace NewsDao.services
{
    public interface IMetadataService:IBaseService<Metadata>
    {
        IQueryable<Metadata> SelectSimpleQuery(CriteriaMetadata criteria);
        List<MetadataDto> SelectSimpleDto(CriteriaMetadata criteria);
    }
    public class MetadataService: BaseService<Metadata>, IMetadataService
    {
        public IQueryable<Metadata> SelectSimpleQuery(CriteriaMetadata criteria)
        {
            var query = Query();

            return query;
        }
        public List<MetadataDto> SelectSimpleDto(CriteriaMetadata criteria)
        {
            var dto = SelectSimpleQuery(criteria).Select(x=> new MetadataDto()
            {
                Id = x.Id,
                RssPath = x.RssPath,
                CompanyId = x.CompanyId,
                CategoryId = x.CategoryId,
                CountryId = x.CountryId,
                Company = x.Company.Name,
                Category = x.Category.Title,
                Country = x.Country.Title
            }).ToList();
            return dto;
        }
        public MetadataService(ApplicationDbContext context) : base(context)
        {
        }
    }
    public class CriteriaMetadata
    {
        public int Id { get; set; }
        public string RssPath { get; set; }
        public int CompanyId { get; set; }
       
        public int CategoryId { get; set; }
        
        public int CountryId { get; set; }
        

        public DateTime? LastUpdate { get; set; }
    }
}
