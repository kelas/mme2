﻿using System;
using System.Collections.Generic;
using System.Text;
using NewsDao.Context;
using NewsDao.entities;
using System.Linq;
using NewsDao.dto;

namespace NewsDao.services
{
    public interface ICompanyService:IBaseService<Company>
    {
        IQueryable<Company> SelectSimpleQuery(CrteriaCompany criteria);
        List<CompanyDto> SelectSimpleDto(CrteriaCompany criteria);
    }
    public class CompanyService: BaseService<Company>, ICompanyService
    {
        public CompanyService(ApplicationDbContext context) : base(context)
        {
        }
        public IQueryable<Company> SelectSimpleQuery(CrteriaCompany criteria)
        {
            var query = Query();

            return query;
        }
        public List<CompanyDto> SelectSimpleDto(CrteriaCompany criteria)
        {
            var dto = SelectSimpleQuery(criteria).Select(x => new CompanyDto
            {
                Id = x.Id,
                LogoPath = x.LogoPath,
                Name = x.Name,
                UserName = x.UserName,
                CountryId = x.CountryId

            }).ToList();
            return dto;
        }


    }
    public class CrteriaCompany
    {
        public int Id { get; set; }
    }
}
