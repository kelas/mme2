﻿using System;
using System.Collections.Generic;
using System.Text;
using NewsDao.Context;
using NewsDao.entities;
using System.Linq;
using NewsDao.dto;

namespace NewsDao.services
{
    public interface ICategoryService : IBaseService<Category>
    {
        IQueryable<Category> SelectSimpleQuery(CriteriaCategory criteria);
        List<CategoryDto> SelectSimpleDto(CriteriaCategory criteria);
    }
    public class CategoryService: BaseService<Category>, ICategoryService
    {
        public CategoryService(ApplicationDbContext context) : base(context)
        {
        }

        public IQueryable<Category> SelectSimpleQuery(CriteriaCategory criteria)
        {
            var query = Query();

            return query;
        }
        public List<CategoryDto> SelectSimpleDto(CriteriaCategory criteria)
        {
            var dto = SelectSimpleQuery(criteria).Select(x => new CategoryDto()
            {
                Id = x.Id,
                Title =x.Title,
                IsActive = x.IsActive

            }).ToList();
            return dto;
        }

    }
    public class CriteriaCategory
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public bool IsActive { get; set; }
    }
}
