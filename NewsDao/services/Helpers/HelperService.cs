﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewsDao.services.Helpers
{
    
    public static class HelperService
    {
         public static bool IfContainOnlyDigits(string str)
         {
             return str.All(c=>char.IsDigit(c));
         }
        public static string ReplaceLangFromatingStr(string str)
        {
            if (str.Contains("μμ"))
            {
                str = str.Replace("μμ","");
            }
            if (str.Contains("πμ"))
            {
                str = str.Replace("πμ", "");
            }
            return str;
        }
    }
}
