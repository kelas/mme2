﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace NewsDao.services.Helpers
{
    public interface IHttpClientService{
         System.Threading.Tasks.Task<string> GetSimpleStringResponse(string url);
    }
    public class HttpClientService: IHttpClientService
    {
        public async System.Threading.Tasks.Task<string> GetSimpleStringResponse(string url)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    var responseMessage = await client.GetAsync(url);
                    var responseString = await responseMessage.Content.ReadAsStringAsync();
                    return responseString;
                }
            }
            catch (Exception ex)
            {

            }
            return null;
        }

    }
}
