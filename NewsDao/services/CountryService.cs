﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NewsDao.Context;
using NewsDao.entities;
using System.Linq;
using NewsDao.dto;

namespace NewsDao.services
{
    public interface ICountryService : IBaseService<Country>
    {
        IQueryable<Country> SelectSimpleQuery(CriteriaCountry criteria);
        List<CountryDto> SelectSimpleDto(CriteriaCountry criteria);
    }
    public class CountryService : BaseService<Country>, ICountryService
    {
        private ApplicationDbContext _context;

        public CountryService(ApplicationDbContext context) : base(context)
        {
            
        }

        public IQueryable<Country> SelectSimpleQuery(CriteriaCountry criteria)
        {
            var query = Query();

            return query;
        }

        public List<CountryDto>SelectSimpleDto(CriteriaCountry criteria)
        {
            var dto = SelectSimpleQuery(criteria).Select(x => new CountryDto()
            {
                Id = x.Id,
                Title = x.Title,
                IsActive = x.IsActive
            }).ToList();
            return dto;
        }

    }

    public class CriteriaCountry
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int IsActive { get; set; }
    }

}
