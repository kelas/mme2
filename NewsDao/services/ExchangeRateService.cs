﻿using NewsDao.Context;
using NewsDao.entities;
using NewsDao.services.Helpers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewsDao.services
{
    public interface IExchangeRateService:IBaseService<ExchangeRate>
    {
        System.Threading.Tasks.Task<string> PopulateDataAsync(string url, string currency);
        System.Threading.Tasks.Task<string> GetOilData(string url);
        IQueryable<ExchangeRate> SelectSimpleQuery(CriteriaExchangeRate criteria);
    }
    public class ExchangeRateService:BaseService<ExchangeRate>, IExchangeRateService
    {
        private readonly IHttpClientService _httpClientService;
        private ApplicationDbContext cnx;
        public ExchangeRateService(ApplicationDbContext context, IHttpClientService httpClientService) : base(context)
        {
            _httpClientService = httpClientService;
            cnx = context;
        }
        public IQueryable<ExchangeRate> SelectSimpleQuery(CriteriaExchangeRate criteria)
        {
            var query = Query();

            if (!string.IsNullOrEmpty(criteria.Rate))
            {
                query = query.Where(x => x.Rate == criteria.Rate);
            }

            return query;
        }

        public async System.Threading.Tasks.Task<string> PopulateDataAsync(string url, string currency)
        {
            try
            {
                var currency_String = await _httpClientService.GetSimpleStringResponse(url);
            
                if (!string.IsNullOrEmpty(currency_String))
                {
                    
                        var currency_JSON = JObject.Parse(currency_String);
                        var currency_obj = currency_JSON[currency];
                        var currency_val = currency_obj["val"].ToString();
                        return currency_val;                                    
                }
                
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public async System.Threading.Tasks.Task<string> GetOilData(string url)
        {
            try
            {
                var oil_String = await _httpClientService.GetSimpleStringResponse(url);
                int i = oil_String.IndexOf("\"B\",") + 3;
                var data = oil_String.Substring(i+1,6);
                return data;
            }
            catch(Exception ex)
            {

            }
            return null;
        }
        
    }

        
    public class CriteriaExchangeRate
    {
        public int Id { get; set; }
        public string Rate { get; set; }
        public decimal Value { get; set; }
    }
}
