﻿using NewsDao.dto;
using NewsDao.entities;
using NewsDao.services.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Xml.Linq;

namespace NewsDao.services.RSS
{
    public interface IRssReedFeed
    {
        System.Threading.Tasks.Task<IEnumerable<Feed>> ReedRssAsync(string feedUrl);
    }
    public class RssReedFeed: IRssReedFeed
    {
        

        public IEnumerable<Feed> ReadRssChanel(XDocument doc)
        {
            var feedItems = doc.Root.Descendants().FirstOrDefault(i => i.Name.LocalName == "channel").Elements().Where(i => i.Name.LocalName == "item").Select(item => new Feed
            {
                Content = item.Elements().First(i => i.Name.LocalName == "description").Value,
                Link = item.Elements().First(i => i.Name.LocalName == "link").Value,
                // PublishDate = DateTime.Parse((item.Elements().First(i => i.Name.LocalName == "pubDate").Value)),
                PublishDate = HelperService.IfContainOnlyDigits(item.Elements().First(i => i.Name.LocalName == "pubDate").Value) ? new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(Convert.ToDouble(item.Elements().First(i => i.Name.LocalName == "pubDate").Value)) : DateTime.Parse(HelperService.ReplaceLangFromatingStr(item.Elements().First(i => i.Name.LocalName == "pubDate").Value)),
                Title = item.Elements().First(i => i.Name.LocalName == "title").Value
            });
            return feedItems;
        }
        public IEnumerable<Feed> ReadRssEntry(XDocument xdoc)
        {
            XNamespace atom = "http://www.w3.org/2005/Atom";
            XNamespace ns = XNamespace.Get("http://www.w3.org/2005/Atom");
            var xmlFeed = from post in xdoc.Descendants(ns + "entry")
                          select new Feed
                          {
                              Title = post.Element(ns + "title").Value,
                              PublishDate = DateTime.Parse(post.Element(ns + "published").Value),
                              Link = post.Element(ns + "link").LastAttribute.Value,
                              Content = post.Element(ns + "summary").Value
                          };
            return xmlFeed;
        }

        public async System.Threading.Tasks.Task<string> ReadAsString(HttpResponseMessage responseMessage)
        {
            try
            {
                var responseString = await responseMessage.Content.ReadAsStringAsync();
                return responseString;
            }
            catch (Exception ex)
            {
                return null;
            }

            return null;
        }
        public async System.Threading.Tasks.Task<string> ReadAsUTF8String(HttpResponseMessage responseMessage)
        {
            try
            {
                var responseStringByte = await responseMessage.Content.ReadAsByteArrayAsync();
                var responseString = System.Text.Encoding.UTF8.GetString(responseStringByte);
                return responseString;
            }
            catch (Exception ex)
            {
                return null;
            }

            return null;
        }
        public async System.Threading.Tasks.Task<IEnumerable<Feed>> ReedRssAsync(string feedUrl)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = new Uri(feedUrl);
                    var responseMessage = await client.GetAsync(feedUrl);
                    //var responseStringByte = await responseMessage.Content.ReadAsByteArrayAsync();
                    var responseString = await ReadAsString(responseMessage);
                    if (string.IsNullOrEmpty(responseString))
                    {
                        responseString = await ReadAsUTF8String(responseMessage);
                    }

                    XDocument doc = XDocument.Parse(responseString);
                    //var test = doc.Root.Descendants();
                    //var feedItems = new IEnumerable<Feed>();
                    if (doc.Root.Descendants().FirstOrDefault(i => i.Name.LocalName == "channel") != null)
                    {
                        var feedItems = ReadRssChanel(doc);
                        return  feedItems;
                    }else if (doc.Root.Descendants().FirstOrDefault(i => i.Name.LocalName == "entry") != null)
                    {
                       
                        var feedItems2 = ReadRssEntry(doc);
                        return  feedItems2;
                    }

                    
                    return null;
                }
                catch (Exception ex)
                {

                }
                return null;
                
            }
        }

    }
}
