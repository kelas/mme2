﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using NewsDao.Context;

namespace NewsDao.services
{
    public interface IBaseService<T> : IDisposable where T : class, new()
    {
        IQueryable<T> Query();
        T GetById(int id);
        T SaveOrUpdate(T entity, string keyField = "Id");
        T Add(T entity);
        T Update(T entity);
        IList<T> SelectAll();
        void RemoveEntity(T entity);
    }
    public abstract class BaseService<T> : IBaseService<T> where T : class, new()
    {
       // private ApplicationDbContext _context = new ApplicationDbContext(new DbContextOptions<ApplicationDbContext>());
        
        private ApplicationDbContext _context;

        protected BaseService(ApplicationDbContext context)
        {
            _context = context;
        }

        public void Dispose()
        {
            DisposeContext();
        }

        public void DisposeContext()
        {
            if (_context == null) return;
            _context.Dispose();
            _context = null;
        }

        public IQueryable<T> Query()
        {
            if(_context == null)
            {
                return null;
            }
            
            var dbSet = _context.Set<T>();
            return dbSet.AsNoTracking();
        }

        public T GetById(int id)
        {
            var dbSet = _context.Set<T>();
            var entity = dbSet.Find(id);
            return entity;
        }
        public object GetFieldValue(T entity, string keyField = "Id")
        {
            return entity.GetType().GetProperty(keyField)?.GetValue(entity, null);
        }

        public virtual T Update(T entity)
        {
            
                var entry = _context.Entry(entity);
                entry.State = EntityState.Modified;
                _context.SaveChanges();
                return entity;
            
        }
        public virtual T Add(T entity)
        {
            if (_context == null) return entity;
            var dbSet = _context.Set<T>();
            dbSet.Add(entity);
            _context.SaveChanges();
            return entity;
        }
        public T SaveOrUpdate(T entity, string keyField = "Id")
        {
            var id = Convert.ToInt32(GetFieldValue(entity, keyField));
            if (id == 0)
                return Add(entity);
            return Update(entity);
        }

        public virtual IList<T> SelectAll()
        {
            var query = Query();
            return query.ToList();
        }

        public virtual void RemoveEntity(T entity)
        {
            
            var dbSet = _context.Set<T>();
            dbSet.Remove(entity);
            _context.SaveChanges();
        }
    }
}
