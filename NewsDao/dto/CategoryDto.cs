﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewsDao.dto
{
    public class CategoryDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public bool IsActive { get; set; }
    }
}
