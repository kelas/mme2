﻿using NewsDao.entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace NewsDao.dto
{
    public class MetadataDto
    {
        public int Id { get; set; }
        public string RssPath { get; set; }
        public int CompanyId { get; set; }
        public string Company { get; set; }
        public int CategoryId { get; set; }
        public string Category { get; set; }
        public int CountryId { get; set; }
        public string Country { get; set; }

        public DateTime? LastUpdate { get; set; }
    }
}
