﻿using NewsDao.entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace NewsDao.dto
{
    public class RssDataDto
    {
        public int Id { get; set; }
        public string Link { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime PublishDate { get; set; }
        public string Logo { get; set; }

        public int CompanyId { get; set; }
        public int CategoryId { get; set; }
        public int CountryId { get; set; }
    }
}
