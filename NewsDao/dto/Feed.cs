﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewsDao.dto
{
    public class Feed
    {
        public int Id { get; set; }
        public string Link { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime PublishDate { get; set; }

    }
}
