﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewsDao.dto
{
    public class ExchangeRateDto
    {
        public int Id { get; set; }
        public string Rate { get; set; }
        public decimal value { get; set; }
    }
}
