﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewsDao.dto
{
    public class CompanyDto
    {
        public int Id { get; set; }
        public string LogoPath { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public int CountryId { get; set; }

        public static implicit operator CompanyDto(List<CompanyDto> v)
        {
            throw new NotImplementedException();
        }
    }
}
