﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using NewsDao.Account;
using NewsDao.entities;

//using NewsWeb.Models;

namespace NewsDao.Context
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {


        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }


        public DbSet<Category> Category { get; set; }
        public DbSet<Country> Country { get; set; }
        public DbSet<Metadata> Metadata { get; set; }
        public DbSet<Company> Company { get; set; }
        public DbSet<RssData> RssData { get; set; }
        public DbSet<RssDataBackup> RssDataBackup { get; set; }
        public DbSet<ExchangeRate> ExchangeRate { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
